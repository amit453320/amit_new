# How to use Django signals in  your Django application



## Introduction

In this tutorial we will use signals in our django application.Django signals are a way for different parts of a Django application to talk to each other by sending and receiving messages when certain events happen. This helps keep the code organized and modular.

## Prerequisites:

You need following things to start work:

1. Python3 installed
2. Postgresql installed
3. Django installed

## Step 1 — Create a virtual environment to isolate your app dependencies. On windows machine, you can install virtualenv module to create virtual environment and on linux, you can use venv module. I am using venv to create a virtual environment on my linux machine.
```
  $  python -m venv venv/
```

## Step 2 - A folder named venv will be ceated on the location where you ran above command. Let's activate the environment:
```
$  source venv/bin/activate
```
## Step 3 - Now that you are inside your virtual environment, let’s install dependencies that are required for this project:
```
$  pip install django psycopg2-binary
```
Django is the framework that you will be using to develop your web app using python and psycopg2-binary is the python module which works as a adapter between postgresql and django.
## Step 4 - In this step, you will start a new django project by using following command.
```
$ django-admin startproject myproject
```
## Step 5- First enter your project directory and start a new django application by following command
```
$ python manage.py startapp myapp 
```
In next step we will install djangorestframework to use rest framework in our project
## Step 6- Install neccessary libraries for this project using following command

```
$ pip install django-channels
$ pip install daphne
```
Now your project directory should look like this:

## Step 7 - To test your application locally, First enter your project directory and run the server using following commands.
```
$ cd myproject
$ python manage.py runserver
```
## Step 8 - Navigate to http://localhost:8000/ to view the Django welcome screen. It should look like as shown in image:

Kill the server once done by pressing ctrl+z.
## Step 9 - Now, you will create a requirements.txt file in your project's root directory. Use below command to automatically create a requirements.txt and copy all dependencies in it.
```
  $ pip freeze > requirements.txt
 ```
 Your project dependencies should have been included in requirements.txt file as shown in image:
 

## Step 10 - Let's edit 'ALLOWED_HOST' in settings.py file present in myproject directory as shown in image:
For now, You can set ALLOWED_HOST to '*' to let the wsgi allow accepting http request from any domain but in production, this is not recommended.


## Step 11 - Now add your app and daphne module in installed apps in settings.py file as shown in image:

## Step 12 - Add Index view :
Create a templates directory in your myapp directory. Within the templates directory you have just created, create another directory called chat, and within that create a file called index.html to hold the template for the index view as shown in below image :

## Step 13 Add room view :
Create the view template for the room view in myapp/templates/chat/room.html as shown in below image:


## Step 14 - Create the view functions for the index vie and room view. Put the following code in myapp/views.py 

## step 15  Start the Channels development server:

$ python3 manage.py runserver
Go to http://127.0.0.1:8000/chat/ in your browser and to see the index page.

Type in “lobby” as the room name and press enter. You should be redirected to the room page at http://127.0.0.1:8000/chat/lobby/ which now displays an empty chat log.

Type the message “hello” and press enter. Nothing happens. In particular the message does not appear in the chat log. Why?

The room view is trying to open a WebSocket to the URL ws://127.0.0.1:8000/ws/chat/lobby/ but we haven’t created a consumer that accepts WebSocket connections yet. If you open your browser’s JavaScript console, you should see an error that looks like:

```
WebSocket connection to 'ws://127.0.0.1:8000/ws/chat/lobby/' failed: Unexpected response code: 500
```
## Step 16 Write your consumers:
add a new file with name consumers.py in your myapp directry with following code shown in below image:

## Step 17 We need to create a routing configuration for the chat app that has a route to the consumer. Create a new file myapp/routing.py with the following code shown in image:


## Step 18 The next step is to point the main ASGI configuration at the chat.routing module. In myproject/asgi.py we need to add this code :

## Step 19 Enable a channel layer:
we can use a channel layer, we must configure it. Edit the mysite/settings.py file and add a CHANNEL_LAYERS setting to the bottom. It should look like:

## Step 20 We can rewrite the consumer to be asynchronous as shown in below image :



